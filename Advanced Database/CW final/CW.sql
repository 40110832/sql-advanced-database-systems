   DROP TYPE Address force;
   DROP TYPE Name force;
   DROP TYPE Phone_nested force;   
   DROP TYPE Person force;
   DROP TYPE Branch force; 
   DROP TYPE Account force; 
   DROP TYPE Employee force; 
   DROP TYPE Customer force; 
   DROP TABLE branch_table force;
   DROP TABLE account_table force;
   DROP TABLE employee_table force;
   DROP TABLE customer_table force;
   DROP TYPE employeesbranch_ref force;
   DROP TABLE employeesbranch_ref_table force;
   DROP TYPE branchcustaccounts_ref force;
   DROP TABLE branchcustaccounts_ref_table force;
   
		CREATE TYPE Address AS OBJECT
    (street VARCHAR2(20),
    city VARCHAR2(20),
    p_code VARCHAR2(10))
    NOT final;
	/
	
	    CREATE TYPE Name AS OBJECT (
    title VARCHAR2(10),
	firstName VARCHAR2(20),
    surName VARCHAR2(20))
    NOT final;
	/
	
	   CREATE TYPE Phone_nested as table of varchar2(15);
	 /
	 
	
	   CREATE TYPE Person AS OBJECT (
    pname Name,
    paddress Address,
    niNum VARCHAR2(10),
    phone Phone_nested,
	HomePhone VARCHAR2(20))
    NOT final;
	 /
	 
	 
	 
	   CREATE TYPE Branch AS OBJECT (
    bID NUMBER,
    baddress Address,
    bphone VARCHAR2(20))
	final;
	 /	 
	 
	  CREATE TYPE Account AS OBJECT (
    accNum NUMBER,
    accType VARCHAR2(10),
    balance FLOAT,
	inRate FLOAT,
	alimit NUMBER,
	opendate DATE)
	final;
	 /	 
	 
	  CREATE TYPE Employee Under Person (
    empID NUMBER,
	supervisorID REF Employee,
    position VARCHAR2(20),
    salary NUMBER,
	joinDate DATE)
	final;
	 /	
	 
	  CREATE TYPE Customer Under Person (
    custID NUMBER)
	final;
	 /	
	 
	 CREATE TABLE branch_table OF Branch (
    bID PRIMARY KEY,
    CONSTRAINT bstreet_const CHECK (baddress.street IS NOT NULL),
    CONSTRAINT bcity_const CHECK (baddress.city IS NOT NULL),
    CONSTRAINT bpostcode_const CHECK (baddress.p_code IS NOT NULL),
	CONSTRAINT bphone_const UNIQUE (bphone)
	);
	
	 CREATE TABLE account_table OF Account (
    accNum PRIMARY KEY,
    CONSTRAINT atype_const CHECK (accType IN ('current', 'savings')),
    CONSTRAINT inRate_const CHECK (inRate IS NOT NULL)
	);	
	
	 CREATE TABLE employee_table of Employee (
    empID PRIMARY KEY,
    CONSTRAINT eaddress_street_const CHECK (paddress.street IS NOT NULL),
	CONSTRAINT eaddress_city_const CHECK (paddress.city IS NOT NULL),
	CONSTRAINT eaddress_pcode_const CHECK (paddress.p_code IS NOT NULL),
    CONSTRAINT ename_title_const CHECK (pname.title IN ('Mr', 'Mrs', 'Miss', 'Dr')),
    CONSTRAINT ename_fname_const CHECK (pname.firstName IS NOT NULL),
	CONSTRAINT ename_surName_const CHECK (pname.surName IS NOT NULL),  	
	CONSTRAINT eniNum_const UNIQUE (niNum),
	CONSTRAINT eposition_const CHECK (position IN ('Head', 'Manager', 'Project Leader', 'Accountant', 'Cashier'))
	) nested table phone store as phone_nt_table;

	 CREATE TABLE customer_table OF Customer (
    custID PRIMARY KEY,
    CONSTRAINT caddress_street_const CHECK (paddress.street IS NOT NULL),
	CONSTRAINT caddress_city_const CHECK (paddress.city IS NOT NULL),
	CONSTRAINT caddress_pcode_const CHECK (paddress.p_code IS NOT NULL),
    CONSTRAINT cname_title_const CHECK (pname.title IN ('Mr', 'Mrs', 'Miss', 'Dr')),
    CONSTRAINT cname_fname_const CHECK (pname.firstName IS NOT NULL),
	CONSTRAINT cname_surName_const CHECK (pname.surName IS NOT NULL),  	
	CONSTRAINT cniNum_const UNIQUE (niNum)
	)nested table phone store as phone_nt_table2;
	
				-- inserting data into branch_table	
				Insert into branch_table values ('901', Address('Market', 'Edinburgh', 'EH1 5AB'), '1311235560');
				Insert into branch_table values ('902', Address('Princess', 'Edinburgh', 'EH10 6AH'), '1311235682');
				Insert into branch_table values ('903', Address('Queen', 'Edinburgh', 'EH8 5AG'), '1311235804');
				Insert into branch_table values ('904', Address('Blair', 'Edinburgh', 'EH9 8AB'), '1311235926');
				Insert into branch_table values ('905', Address('High', 'Edinburgh', 'EH9 6EJ'), '1311236048');
				Insert into branch_table values ('906', Address('Grove', 'Edinburgh', 'EH7 5AR'), '1311236170');
				Insert into branch_table values ('907', Address('Garden', 'Edinburgh', 'EH12 5DR'), '1311236292');
				Insert into branch_table values ('908', Address('Bridge', 'Glasgow', 'G18 1QQ'), '1311236414');
				Insert into branch_table values ('909', Address('Berkeley', 'Glasgow', 'G3 9BA'), '1311236536');
				Insert into branch_table values ('910', Address('Dorset', 'Glasgow', 'G8 9HH'), '1311236658');
				Insert into branch_table values ('911', Address('Nelson', 'Glasgow', 'G4 9AB'), '1311236780');
				Insert into branch_table values ('912', Address('Centre', 'Glasgow', 'G6 6BT'), '1311236902');
				Insert into branch_table values ('913', Address('Glenfarg', 'Glasgow', 'G1 8EN'), '1311237024');
				Insert into branch_table values ('914', Address('Meadowside', 'Dundee', ' DD1 1LH'), '1311237146');
				Insert into branch_table values ('915', Address('Hilltown', 'Dundee', 'DD1 7HG'), '1311237268');
				Insert into branch_table values ('916', Address('Kinghorne', 'Dundee', 'DD2 8RT'), '1311237390');
				Insert into branch_table values ('917', Address('Macaulay', 'Dundee', 'DD2 6AH'), '1311237512');
				Insert into branch_table values ('918', Address('Lochbank', 'Livingston', 'EH54 7AG'), '1311237634');
				Insert into branch_table values ('919', Address('Katherine', 'Livingston', 'EH54 3AF'), '1311237756');
				Insert into branch_table values ('920', Address('Rushbank', 'Livingston', 'EH54 9TU'), '1311237878');  

				-- inserting data into account_table
                Insert into account_table values ('1001', 'current',820.5,0.005, '800', to_date('2011/05/01','YYYY/MM/DD'));
				Insert into account_table values ('1010', 'savings',3122.2,0.02, NULL, to_date('2010/03/08','YYYY/MM/DD'));
				Insert into account_table values ('8002', 'current',200,0.005, '100', to_date('2009/05/05','YYYY/MM/DD'));
				Insert into account_table values ('8014', 'savings',982.5,0.010, '900', to_date('2008/03/03','YYYY/MM/DD'));
				Insert into account_table values ('8026', 'current',1765,0.02, '1700', to_date('2006/05/07','YYYY/MM/DD'));
				Insert into account_table values ('8038', 'savings',2547.5,0.005, '2500', to_date('2009/05/08','YYYY/MM/DD'));
				Insert into account_table values ('8050', 'current',3330,0.010, '3300', to_date('2012/01/02','YYYY/MM/DD'));
				Insert into account_table values ('8062', 'savings',4112.5,0.02, NULL, to_date('2008/08/05','YYYY/MM/DD'));
				Insert into account_table values ('8074', 'current',4895,0.005, '4900', to_date('2006/06/05','YYYY/MM/DD'));
				Insert into account_table values ('8086', 'savings',5677.5,0.010, '5700', to_date('2004/04/05','YYYY/MM/DD'));
				Insert into account_table values ('8098', 'current',6460,0.02, '6500', to_date('2003/02/02','YYYY/MM/DD'));
				Insert into account_table values ('9234', 'savings',7242.5,0.005, '7300', to_date('2001/02/03','YYYY/MM/DD'));
				Insert into account_table values ('9246', 'current',8025,0.010, NULL, to_date('2013/09/01','YYYY/MM/DD'));
				Insert into account_table values ('9258', 'savings',8807.5,0.02, '8900', to_date('2015/02/02','YYYY/MM/DD'));
				Insert into account_table values ('9270', 'current',9560,0.005, '9700', to_date('2001/02/01','YYYY/MM/DD'));
				Insert into account_table values ('9282', 'savings',10372.5,0.010, '10500', to_date('2016/01/01','YYYY/MM/DD'));
				Insert into account_table values ('9294', 'current',11155,0.02, '11300', to_date('2015/08/02','YYYY/MM/DD'));
				Insert into account_table values ('9306', 'savings',11937.5,0.005, '12100', to_date('2009/11/25','YYYY/MM/DD'));
				Insert into account_table values ('9318', 'current',12720,0.010, '12900', to_date('2010/04/04','YYYY/MM/DD'));
				Insert into account_table values ('9330', 'savings',13502.5,0.02, NULL, to_date('2014/12/15','YYYY/MM/DD'));

				-- inserting data into employee_table				
				Insert into employee_table values (Name('Mrs','Alison','Smith'), Address('Dart','Edinburgh','EH10 5TT'), 'NI001', Phone_nested('07705623443','07907812345'), '01312125555', '101',NULL, 'Head', '50000', to_date('2006/02/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','John','William'), Address('New','Edinburgh','EH24 AB'), 'NI010', Phone_nested('07902314551','07701234567'), '01312031990', '105',NULL, 'Manager', '40000', to_date('2007/03/04','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Mark','Slack'), Address('Old','Edinburgh','EH94 BB'), 'NI180', Phone_nested(), '01312102211', '108',NULL, 'Accountant', '30000', to_date('2009/02/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Miss','Lena','Donaldson'), Address('Bridge','Edinburgh','EH4 CB'), 'NI121', Phone_nested('07902314766','07701234346'), '01312105212', '104',NULL, 'Project Leader', '35000', to_date('2009/06/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Brian','Conner'), Address('Newington','Edinburgh','EH9 CC'), 'NI122', Phone_nested('07902314545','07701234098'), '01312106213', '111',NULL, 'Cashier', '20000', to_date('2009/07/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Kalan','Jr'), Address('Powlarth','Edinburgh','EH13 CA'), 'NI123', Phone_nested('07902314573','07701234264'), '01312107214', '112',NULL, 'Cashier', '20000', to_date('2009/08/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Maria','Nicolas'), Address('Morningside','Edinburgh','EH8 AB'), 'NI124', Phone_nested('07902314521','07701234111'), '01312108215', '113',NULL, 'Cashier', '20000', to_date('2009/09/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Pippa','Anderson'), Address('Dart','Edinburgh','EH8 AD'), 'NI125', Phone_nested('07902314787'), '01312103267', '114',NULL, 'Cashier', '20000', to_date('2009/09/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Aaron','Mark'), Address('Bridge','Edinburgh','EH11 EA'), 'NI808', Phone_nested('07705623124','07701234562'), '01312105675', '801',NULL, 'Head', '50000', to_date('2004/09/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Abram','James'), Address('Nelson','Edinburgh','EH12 EB'), 'NI809', Phone_nested('07902314729','07701235846'), '01312106755', '805',NULL, 'Manager', '40000', to_date('2005/02/01','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Jack','Smith'), Address('Adam','Edinburgh','EH16 CA'), 'NI810', Phone_nested('0781209185','07701239678'), '01311112223', '804',NULL, 'Project Leader', '35000', to_date('2003/01/14','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Nicola','Bailey'), Address('Hilton','Edinburgh','EH5 CB'), 'NI811', Phone_nested('0781209891','07701239444'), '01311111451', '808',NULL, 'Accountant', '30000', to_date('2006/05/14','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Eva','Parker'), Address('Bank','Edinburgh','EH2 CC'), 'NI812', Phone_nested(), '01311113567', '811',NULL, 'Cashier', '20000', to_date('2005/05/16','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Liam','Watson'), Address('Queen','Edinburgh','EH18 CA'), 'NI813', Phone_nested(), '01311112964', '812',NULL, 'Cashier', '20000', to_date('2003/02/19','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Ethan','Proto'), Address('High','Glasgow','G6 6BT'), 'NI1145', Phone_nested('0781209123','07701239234'), '01311156789', '1101',NULL, 'Head', '50000', to_date('2007/03/13','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Michael','Hempstead'), Address('Market','Glasgow','G2 6BT'), 'NI1146', Phone_nested('0781209345','07701239333'), '01311158385', '1105',NULL, 'Manager', '40000', to_date('2008/01/11','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','James','Berfield'), Address('Fountain','Glasgow','G3 6BT'), 'NI1147', Phone_nested('0781202345','07701239234'), '01311113743', '1104',NULL, 'Project Leader', '35000', to_date('2009/03/12','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Rosie','Crossen'), Address('Lake','Glasgow','G4 6BT'), 'NI1148', Phone_nested('0781289006','07701239975'), '01311112366', '1108',NULL, 'Accountant', '30000', to_date('2010/02/19','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mrs','Alice','Harley Weber'), Address('Garden','Glasgow','G6 2BT'), 'NI1149', Phone_nested('0781209835'), '01311112933', '1111',NULL, 'Cashier', '20000', to_date('2008/07/07','YYYY/MM/DD'));
				Insert into employee_table values (Name('Mr','Daniel','Cebula'), Address('Centre','Glasgow','G6 7AC'), 'NI1150', Phone_nested(), '01311113467', '1112',NULL, 'Cashier', '20000', to_date('2008/07/08','YYYY/MM/DD'));
			
				-- update ref into employee_table			
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '101') where e.empID = '105';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '105') where e.empID = '108';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '105') where e.empID = '104';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '108') where e.empID = '111';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '108') where e.empID = '112';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '108') where e.empID = '113';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '108') where e.empID = '114';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '801') where e.empID = '805';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '805') where e.empID = '804';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '805') where e.empID = '808';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '808') where e.empID = '811';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '808') where e.empID = '812';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '1101') where e.empID = '1105';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '1105') where e.empID = '1104';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '1105') where e.empID = '1108';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '1108') where e.empID = '1111';
				Update EMPLOYEE_TABLE e set e.supervisorID = (select ref(e) from EMPLOYEE_TABLE e WHERE e.empID = '1108') where e.empID = '1112';
		
			
				-- inserting data into customer_table					
	            Insert into customer_table values (Name('Mr','Jack','Smith'), Address('EH1 6EA','Adam','Edinburgh'), 'NI810', Phone_nested('0781209890','0771234567'), '01311112223', '1002');
				Insert into customer_table values (Name('Mrs','Anna','Smith'), Address('EH1 6EA','Adam','Edinburgh'), 'NI011', Phone_nested('07701112222'), '01311112223', '1003');
				Insert into customer_table values (Name('Mr','Liam','Bain'), Address('EH2 8XN','New','Edinburgh'), 'NI034', Phone_nested(), '01314425567', '1098');
				Insert into customer_table values (Name('Mr','Lincoln',' Kirby'), Address('EH8 AD','Meadowside','Edinburgh'), 'NI345', Phone_nested('07902314245','07701231234'), '01312105477', '1023');
				Insert into customer_table values (Name('Mr','Weston','Mazurek'), Address('EH14 EA','Hilltown','Edinburgh'), 'NI213', Phone_nested('07902314246'), '01312105478', '1024');
				Insert into customer_table values (Name('Mr','Jonathan','Lach'), Address('EH12 EB','Kinghorne','Edinburgh'), 'NI423', Phone_nested('07902314463'), '01312105479', '1025');
				Insert into customer_table values (Name('Mr','Mckinley','Clay'), Address('EH16 CA','Macaulay','Edinburgh'), 'NI128', Phone_nested('07902315666','07701236345'), '01312105480', '1026');
				Insert into customer_table values (Name('Mr','Shane','Lough'), Address('EH6 CB','Old','Edinburgh'), 'NI185', Phone_nested('07902314456','07701237456'), '01312105481', '1031');
				Insert into customer_table values (Name('Mr','Zachery','Bembry'), Address('EH3 CC','Hilton','Edinburgh'), 'NI156', Phone_nested('07902314234'), '01312105482', '1036');
				Insert into customer_table values (Name('Mr','Samual','Watrous'), Address('EH11 5TT','Bank','Edinburgh'), 'NI143', Phone_nested('07902314123','07701234567'), '01312105483', '1041');
				Insert into customer_table values (Name('Mr','Jermaine','Dawson'), Address('EH28 AB','Queen','Edinburgh'), 'NI144', Phone_nested('07902314234','07701237464'), '01312105484', '1046');
				Insert into customer_table values (Name('Mr','Reinaldo','Youngblood'), Address('EH9 BF','High','Edinburgh'), 'NI912', Phone_nested('07902314345'), '01312105346', '1051');
				Insert into customer_table values (Name('Mr','Jess','Dona'), Address('EH1 CB','Market','Edinburgh'), 'NI913', Phone_nested('07902314456','07701234567'), '01312105347', '1056');
				Insert into customer_table values (Name('Mrs','Rosann','Rayes'), Address('EH2 CC','Fountain','Edinburgh'), 'NI444', Phone_nested('07902314567','07701236789'), '01312105348', '1061');
				Insert into customer_table values (Name('Mrs','Margareta','Epperson'), Address('EH18 CC','Lake','Edinburgh'), 'NI555', Phone_nested('07902314789','07701278996'), '01312105349', '1066');
				Insert into customer_table values (Name('Mrs','Roxanna','Groves'), Address('G5 9BA','Princess','Glasgow'), 'NI666', Phone_nested('07902314890','07701236343'), '01312105350', '1071');
				Insert into customer_table values (Name('Miss','Shawnee','Bunch'), Address('G6 9BB','Queen','Glasgow'), 'NI777', Phone_nested('07902314876','07701234889'), '01312123456', '1076');
				Insert into customer_table values (Name('Miss','Ewa','Damron'), Address('G15 9AB','Blair','Glasgow'), 'NI788', Phone_nested('07902314643'), '01312123457', '1081');
				Insert into customer_table values (Name('Mrs','Narcisa','Damron'), Address('G15 9AB','Blair','Glasgow'), 'NI114', Phone_nested('07902314423','07701234965'), '01312123457', '1086');
				Insert into customer_table values (Name('Mrs','Ardelia','Quint'), Address('G3 8EN','Grove','Glasgow'), 'NI119', Phone_nested('07902314566','07701234075'), '01312123459', '1091');
				Insert into customer_table values (Name('Mrs','Belle','Raap'), Address('G17 6BT','Garden','Glasgow'), 'NI120', Phone_nested('07902314345'), '01312123460', '1096');
	                   	
	
	create type employeesbranch_ref as object (
branchref ref Branch,
employeeref ref Employee
);
/

create table employeesbranch_ref_table of employeesbranch_ref;

			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '101';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '105';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '108';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '104';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '111';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '112';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '113';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '901' AND e.empID = '114';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '801';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '805';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '804';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '808';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '811';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '902' AND e.empID = '812';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1101';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1105';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1104';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1108';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1111';
			INSERT INTO employeesbranch_ref_table SELECT REF(b), REF(e) FROM branch_table b, employee_table e WHERE b.bID = '908' AND e.empID = '1112';


 
 	create type branchcustaccounts_ref as object (
branchcustref ref Branch,
custref ref Customer,
accref ref Account
);
/

create table branchcustaccounts_ref_table of branchcustaccounts_ref;

			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1002' AND a.accNum = '1010';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1003' AND a.accNum = '1010';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1098' AND a.accNum = '8002';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1023' AND a.accNum = '8014';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1024' AND a.accNum = '8026';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1025' AND a.accNum = '8038';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1026' AND a.accNum = '8050';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1031' AND a.accNum = '8062';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1036' AND a.accNum = '8074';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1041' AND a.accNum = '8086';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '901' AND c.custID = '1046' AND a.accNum = '8098';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '902' AND c.custID = '1051' AND a.accNum = '9234';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '902' AND c.custID = '1056' AND a.accNum = '9246';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '902' AND c.custID = '1061' AND a.accNum = '9258';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1066' AND a.accNum = '9270';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1071' AND a.accNum = '9282';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1076' AND a.accNum = '9294';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1081' AND a.accNum = '9306';
			INSERT INTO branchcustaccounts_ref_table SELECT REF(b), REF(c), REF(a) FROM branch_table b, customer_table c, account_table a WHERE b.bID = '908' AND c.custID = '1086' AND a.accNum = '9306';

			--queries 4.a
			--Find employees whose first name includes the string “on” and live in
            --Edinburgh, displaying their full names (not types). 
			SELECT e.pname.title, e.pname.firstName, e.pname.surName FROM employee_table e WHERE e.pname.firstName LIKE '%on%';
			--queries 4.b
			-- Find the number of saving accounts at each branch, displaying the
            --number and branch’s address (not the type). 
			SELECT COUNT(b.accref.accType), b.branchcustref.baddress.street, b.branchcustref.baddress.city, b.branchcustref.baddress.p_code FROM branchcustaccounts_ref_table b WHERE  b.accref.accType LIKE 'current' Group BY b.branchcustref.baddress.street, b.branchcustref.baddress.city, b.branchcustref.baddress.p_code ;
            --queries 4.c
			--For all customers in the bank, find customers who have the highest
			--balance in their savings account for each of the branches, displaying
			--their names, the balance, the branch ID and the free overdraft limit in
			--their current accounts.  not showing names
			SELECT  MAX(b.accref.balance), b.branchcustref.bID FROM branchcustaccounts_ref_table b WHERE  b.accref.accType LIKE 'savings' Group BY b.branchcustref.bID;
			--queries 4.d
			--Find employees who are supervised by a manager and have accounts in
			--the bank, displaying the branch address that the employee works in and
			--the branch address that the account is opened with.
			SELECT b.branchcustref.baddress.street, b.branchcustref.baddress.city , b.branchcustref.baddress.p_code, e.branchref.baddress.street, e.branchref.baddress.city, e.branchref.baddress.p_code FROM branchcustaccounts_ref_table b Inner JOIN employeesbranch_ref_table e ON e.employeeref.niNum = b.custref.niNum Where e.employeeref.supervisorID.position like 'Manager';
			--queries 4.f     
            --Find customers who have more than one mobile, and at least one of the
			--numbers starts with 0770, displaying the customer’s full name (not the
			--type) and mobile numbers. You are required to use COLLECTIONS.Doesn't show the numbers			
			SELECT c.pname.title, c.pname.firstName, c.pname.surName from customer_table c, table(c.phone) t Where t.column_value LIKE '%0770%' OR t.column_value LIKE '%0790%' GROUP BY c.pname.title, c.pname.firstName, c.pname.surName Having COUNT(*)>1; 	
			--queries 4.g	
			--Find the number of employees who are supervised by Mr William,
			--who is supervised by Mrs Smith. You are required to use REFERENCES
			Select COUNT(value(e)) from employee_table e Where e.supervisorID.pname.surName = 'William';
			
			
			
   DROP TYPE Address force;
   DROP TYPE Name force;
   DROP TYPE Phone_nested force;   
   DROP TYPE Person force;
   DROP TYPE Branch force; 
   DROP TYPE Account force; 
   DROP TYPE Employee force; 
   DROP TYPE Customer force; 
   DROP TABLE branch_table force;
   DROP TABLE account_table force;
   DROP TABLE employee_table force;
   DROP TABLE customer_table force;
   DROP TYPE branchemployees_ref force;
   DROP TABLE branchemployees_ref_table force;
   DROP TYPE branchcustaccounts_ref force;
   DROP TABLE branchcustaccounts_ref_table force;
   
		CREATE TYPE Address AS OBJECT
    (street VARCHAR2(20),
    city VARCHAR2(20),
    p_code VARCHAR2(10))
    NOT final;
	/
	
	    CREATE TYPE Name AS OBJECT (
    title VARCHAR2(30),
	firstName VARCHAR2(30),
    surName VARCHAR2(20))
    NOT final;
	/
	
	   CREATE TYPE Phone_nested as table of varchar2(15);
	 /
	 
	
	   CREATE TYPE Person AS OBJECT (
    pname Name,
    paddress Address,
    niNum NUMBER,
    phone Phone_nested,
	HomePhone VARCHAR2(20))
    NOT final;
	 /
	 
	 
	 
	   CREATE TYPE Branch AS OBJECT (
    bID NUMBER,
    baddress Address,
    bphone VARCHAR2(20))
	final;
	 /	 
	 
	  CREATE TYPE Account AS OBJECT (
    accNum NUMBER,
    accType VARCHAR2(10),
    balance FLOAT,
	inRate FLOAT,
	alimit NUMBER,
	opendate DATE)
	final;
	 /	 
	 
	  CREATE TYPE Employee Under Person (
    empID NUMBER,
	supervisorID NUMBER,
    position VARCHAR2(20),
    salary NUMBER,
	joinDate DATE)
	final;
	 /	
	 
	  CREATE TYPE Customer Under Person (
    custID NUMBER)
	final;
	 /	
	 
	 CREATE TABLE branch_table OF Branch (
    bID PRIMARY KEY,
    CONSTRAINT bstreet_const CHECK (baddress.street IS NOT NULL),
    CONSTRAINT bcity_const CHECK (baddress.city IS NOT NULL),
    CONSTRAINT bpostcode_const CHECK (baddress.p_code IS NOT NULL),
	CONSTRAINT bphone_const UNIQUE (bphone)
	);
	
	 CREATE TABLE account_table OF Account (
    accNum PRIMARY KEY,
    CONSTRAINT atype_const CHECK (accType IN ('current', 'savings')),
    CONSTRAINT inRate_const CHECK (inRate IS NOT NULL)
	);	
	
	 CREATE TABLE employee_table of Employee (
    empID PRIMARY KEY,
    CONSTRAINT eaddress_street_const CHECK (paddress.street IS NOT NULL),
	CONSTRAINT eaddress_city_const CHECK (paddress.city IS NOT NULL),
	CONSTRAINT eaddress_pcode_const CHECK (paddress.p_code IS NOT NULL),
    CONSTRAINT ename_title_const CHECK (pname.title IN ('Mr', 'Mrs', 'Miss', 'Dr')),
    CONSTRAINT ename_fname_const CHECK (pname.firstName IS NOT NULL),
	CONSTRAINT ename_surName_const CHECK (pname.surName IS NOT NULL),  	
	CONSTRAINT eniNum_const UNIQUE (niNum),
	CONSTRAINT eposition_const CHECK (position IN ('Head', 'Manager', 'Project Leader', 'Accountant', 'Cashier'))
	) nested table phone store as phone_nt_table;

	 CREATE TABLE customer_table OF Customer (
    custID PRIMARY KEY,
    CONSTRAINT caddress_street_const CHECK (paddress.street IS NOT NULL),
	CONSTRAINT caddress_city_const CHECK (paddress.city IS NOT NULL),
	CONSTRAINT caddress_pcode_const CHECK (paddress.p_code IS NOT NULL),
    CONSTRAINT cname_title_const CHECK (pname.title IN ('Mr', 'Mrs', 'Miss', 'Dr')),
    CONSTRAINT cname_fname_const CHECK (pname.firstName IS NOT NULL),
	CONSTRAINT cname_surName_const CHECK (pname.surName IS NOT NULL),  	
	CONSTRAINT cniNum_const UNIQUE (niNum)
	)nested table phone store as phone_nt_table2;
	
				-- inserting data into branch_table	
				Insert into branch_table values ('901', Address('Market', 'Edinburgh', 'EH1 5AB'), '1311235560');
				Insert into branch_table values ('902', Address('Princess', 'Edinburgh', 'EH10 6AH'), '1311235682');
				Insert into branch_table values ('903', Address('Queen', 'Edinburgh', 'EH8 5AG'), '1311235804');
				Insert into branch_table values ('904', Address('Blair', 'Edinburgh', 'EH9 8AB'), '1311235926');
				Insert into branch_table values ('905', Address('High', 'Edinburgh', 'EH9 6EJ'), '1311236048');
				Insert into branch_table values ('906', Address('Grove', 'Edinburgh', 'EH7 5AR'), '1311236170');
				Insert into branch_table values ('907', Address('Garden', 'Edinburgh', 'EH12 5DR'), '1311236292');
				Insert into branch_table values ('908', Address('Bridge', 'Glasgow', 'G18 1QQ'), '1311236414');
				Insert into branch_table values ('909', Address('Berkeley', 'Glasgow', 'G3 9BA'), '1311236536');
				Insert into branch_table values ('910', Address('Dorset', 'Glasgow', 'G8 9HH'), '1311236658');
				Insert into branch_table values ('911', Address('Nelson', 'Glasgow', 'G4 9AB'), '1311236780');
				Insert into branch_table values ('912', Address('Centre', 'Glasgow', 'G6 6BT'), '1311236902');
				Insert into branch_table values ('913', Address('Glenfarg', 'Glasgow', 'G1 8EN'), '1311237024');
				Insert into branch_table values ('914', Address('Meadowside', 'Dundee', ' DD1 1LH'), '1311237146');
				Insert into branch_table values ('915', Address('Hilltown', 'Dundee', 'DD1 7HG'), '1311237268');
				Insert into branch_table values ('916', Address('Kinghorne', 'Dundee', 'DD2 8RT'), '1311237390');
				Insert into branch_table values ('917', Address('Macaulay', 'Dundee', 'DD2 6AH'), '1311237512');
				Insert into branch_table values ('918', Address('Lochbank', 'Livingston', 'EH54 7AG'), '1311237634');
				Insert into branch_table values ('919', Address('Katherine', 'Livingston', 'EH54 3AF'), '1311237756');
				Insert into branch_table values ('920', Address('Rushbank', 'Livingston', 'EH54 9TU'), '1311237878');  

				-- inserting data into account_table
                Insert into account_table values ('1001', 'current',820.5,0.005, '800', to_date('2011/05/01','YYYY/MM/DD'));
				Insert into account_table values ('1010', 'savings',3122.2,0.02, NULL, to_date('2010/03/08','YYYY/MM/DD'));
				Insert into account_table values ('8002', 'current',200,0.005, '100', to_date('2009/05/05','YYYY/MM/DD'));
				Insert into account_table values ('8014', 'savings',982.5,0.010, '900', to_date('2008/03/03','YYYY/MM/DD'));
				Insert into account_table values ('8026', 'current',1765,0.02, '1700', to_date('2006/05/07','YYYY/MM/DD'));
				Insert into account_table values ('8038', 'savings',2547.5,0.005, '2500', to_date('2009/05/08','YYYY/MM/DD'));
				Insert into account_table values ('8050', 'current',3330,0.010, '3300', to_date('2012/01/02','YYYY/MM/DD'));
				Insert into account_table values ('8062', 'savings',4112.5,0.02, NULL, to_date('2008/08/05','YYYY/MM/DD'));
				Insert into account_table values ('8074', 'current',4895,0.005, '4900', to_date('2006/06/05','YYYY/MM/DD'));
				Insert into account_table values ('8086', 'savings',5677.5,0.010, '5700', to_date('2004/04/05','YYYY/MM/DD'));
				Insert into account_table values ('8098', 'current',6460,0.02, '6500', to_date('2003/02/02','YYYY/MM/DD'));
				Insert into account_table values ('9234', 'savings',7242.5,0.005, '7300', to_date('2001/02/03','YYYY/MM/DD'));
				Insert into account_table values ('9246', 'current',8025,0.010, NULL, to_date('2013/09/01','YYYY/MM/DD'));
				Insert into account_table values ('9258', 'savings',8807.5,0.02, '8900', to_date('2015/02/02','YYYY/MM/DD'));
				Insert into account_table values ('9270', 'current',9560,0.005, '9700', to_date('2001/02/01','YYYY/MM/DD'));
				Insert into account_table values ('9282', 'savings',10372.5,0.010, '10500', to_date('2016/01/01','YYYY/MM/DD'));
				Insert into account_table values ('9294', 'current',11155,0.02, '11300', to_date('2015/08/02','YYYY/MM/DD'));
				Insert into account_table values ('9306', 'savings',11937.5,0.005, '12100', to_date('2009/11/25','YYYY/MM/DD'));
				Insert into account_table values ('9318', 'current',12720,0.010, '12900', to_date('2010/04/04','YYYY/MM/DD'));
				Insert into account_table values ('9330', 'savings',13502.5,0.02, NULL, to_date('2014/12/15','YYYY/MM/DD'));
				
	
	            Insert into employee_table values (Name('Mrs','Alison','Smith'), Address('Dart','Edinburgh','EH105TT'), '1001', Phone_nested('23443','12345'), '25555', '101', NULL, 'Head', '50000', to_date('2011/05/01','YYYY/MM/DD'));
                Insert into customer_table values (Name('Mrs','Alison','Smith'), Address('Dart','Edinburgh','EH105TT'), '1001', Phone_nested('23443','12345'), '25555', '101');
	
	
	create type branchemployees_ref as object (
branchref ref Branch,
employeeref ref Employee
);
/

create table branchemployees_ref_table of branchemployees_ref;

INSERT INTO branchemployees_ref_table
 SELECT REF(b), REF(e)
 FROM branch_table b, employee_table e
 WHERE b.bID = '901'
 AND e.empID = '101';
 
 	create type branchcustaccounts_ref as object (
branchcustref ref Branch,
custref ref Customer,
accref ref Account
);
/

create table branchcustaccounts_ref_table of branchcustaccounts_ref;

INSERT INTO branchcustaccounts_ref_table
 SELECT REF(b), REF(c), REF(a)
 FROM branch_table b, customer_table c, account_table a
 WHERE b.bID = '901'
 AND c.custID = '101'
 AND a.accNum = '1001';
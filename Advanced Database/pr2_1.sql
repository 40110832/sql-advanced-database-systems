  /*
  CREATE TYPE person AS OBJECT (
    name VARCHAR2(30),
    phone VARCHAR2(20) );
	/
	CREATE TABLE person_table OF person;
	/
	CREATE TYPE street AS OBJECT (
    snumber NUMBER,
    sname VARCHAR2(30) );
    /
    CREATE TYPE address AS OBJECT (
     street_and_number street,
     city VARCHAR2(30),
     postal_code VARCHAR2(8));
     /
	 */
	 /*
  DROP TYPE Name force;
  DROP TYPE Address force; 
  DROP TYPE peopleType force;
  Drop TABLE peopleTable force;
	 CREATE TYPE Name AS OBJECT (
    firstname VARCHAR2(30),
    surname VARCHAR2(20) )
    not final;
	/

create type Address as object
    (street varchar2(20),
    city varchar2(20),
    postal_code varchar2(8))
    not final;
	/
  create type peopleType as object
    ( pname Name,
     paddress Address,
     dateOfBirth date)
     not final;
    /
    
  CREATE TABLE peopleTable OF peopleType;
	  
  
  Insert into peopleTable
    values
    (Name('John','Smith'),
    Address('10 Merchiston','Edinburgh','EH10 5DT'),
    to_date('2014/08/18','YYYY/MM/DD')
    );
   
     Insert into peopleTable
    values
    (Name('Adam','Paterson'),
    Address('11 Merchiston','Edinburgh','EH11 5DT'),
    to_date('2014/09/18','YYYY/MM/DD')
    );
	
	 Insert into peopleTable
    values
    (Name('Monika','Belluchi'),
    Address('12 Merchiston','Edinburgh','EH12 5DT'),
    to_date('2014/09/11','YYYY/MM/DD')
    );
	
     Insert into peopleTable
    values
    (Name('Tom','Harrison'),
    Address('13 Merchiston','Edinburgh','EH13 5DT'),
    to_date('2014/09/06','YYYY/MM/DD')
    );
	
	 Insert into peopleTable
    values
    (Name('Leonid','Atkinson'),
    Address('14 Merchiston','Edinburgh','EH14 5DT'),
    to_date('2014/02/06','YYYY/MM/DD')
    );
	
   select p.pname.surname, p.paddress.city from peopleTable p;  
   select p.* from peopleTable p;   
   select p.dateOfBirth from peopleTable p;   
   select p.pname.firstname from peopleTable p; 
   SELECT * FROM peopleTable p;   
   SELECT pname FROM peopleTable;   
   SELECT name.surname FROM peopleTable;
   SELECT p.pname.surname FROM peopleTable p WHERE p.pname.firstname LIKE '%eo%';
   */
   DROP Type Namet force;
   DROP Type Phone force;
   DROP Type Address force;
   DROP Type Persontype force;
   DROP TABLE personTable force;
   
   	 CREATE TYPE Namet AS OBJECT (
    firstname VARCHAR2(30),
	middlename VARCHAR2(30),
    surname VARCHAR2(20) )
    not final;
	/
	
	CREATE TYPE Phone AS OBJECT (
    business Number,
	home Number,
    mobile Number)
    not final;
	/
		create type Address as object
    (street varchar2(20),
    city varchar2(20),
    postal_code varchar2(8))
    not final;
	/
	  create type Persontype as object(
	 pname Namet,
	 pphone Phone,
     paddress Address)
     not final;
    /
		
  CREATE TABLE personTable OF personType;	
  
       Insert into personTable
    values
    (Namet('Tom','Middle', 'Harrison'),
	Phone('123', '1123', '11123'),
    Address('13 Merchiston','Edinburgh','EH13 5DT')
    );	
	       Insert into personTable
    values
    (Namet('Tom','Hardy', 'Jeferson'),
	Phone('124', '1124', '11124'),
    Address('14 Merchiston','Edinburgh','EH14 5DT')
    );
	
		    Insert into personTable
    values
    (Namet('Tom','Bristo;', 'Mongomary'),
	Phone('125', '1125', '11125'),
    Address('15 Merchiston','Edinburgh','EH15 5DT')
    );
	
select * from user_types;
select * from user_objects;
select * from user_source;
  DROP TABLE CustomerAccount;
  DROP TABLE Employee; 
  DROP TABLE Account;
  DROP TABLE Branch;
  DROP TABLE Customer;

 
  CREATE TABLE Branch 
   (bID NUMBER NOT NULL, 
	street VARCHAR2(20), 
	city VARCHAR2(20), 
	p_code VARCHAR2(20),
    bphone NUMBER,
    PRIMARY KEY (bID)
   ) ;
 
  CREATE TABLE Account 
   (accNum NUMBER, 
	accType VARCHAR2(20),
	bID NUMBER NOT NULL,	
	inRate NUMBER,
    limitOfFreeOD NUMBER,
	openDate DATE,
	PRIMARY KEY (AccNum),
	CONSTRAINT fk_account FOREIGN KEY (bID)
    REFERENCES Branch(bID)
   ) ;
   
      CREATE TABLE Employee
    (empID NUMBER,
	 street VARCHAR2(20),
	 city VARCHAR2(20),
	 postCode VARCHAR2(20),
	 firstName VARCHAR2(20),
	 surName VARCHAR2(20),
	 empHomePhone NUMBER,
	 empMobile1 NUMBER,
	 empMobile2 NUMBER,
	 supervisorID NUMBER,
	 title VARCHAR2(20),
	 salary VARCHAR2(20),
	 niNum NUMBER,
	 bID NUMBER,
	 joinDate DATE,
	 PRIMARY KEY (empID),
	 CONSTRAINT fk_employee FOREIGN KEY (bID)
     REFERENCES Branch(bID)
   ) ;
   
        CREATE TABLE Customer
    (custID NUMBER,
	 street VARCHAR2(20),
	 city VARCHAR2(20),
	 postCode VARCHAR2(20),
	 firstName VARCHAR2(20),
	 surName VARCHAR2(20),
	 custHomePhone NUMBER,
	 custMobile1 NUMBER,
	 custMobile2 NUMBER,
	 niNum NUMBER,
	 PRIMARY KEY (custID)
   ) ;
   
         CREATE TABLE CustomerAccount
    (custID NUMBER,	  
     accNum NUMBER,
	 PRIMARY KEY (custID, accNum),
	 CONSTRAINT fk_cust FOREIGN KEY (custID)
     REFERENCES Customer(custID),
	 CONSTRAINT fk_acc FOREIGN KEY (accNum)
     REFERENCES Account(accNum)
     );
     
INSERT INTO Branch(bID, street, city, p_code, bphone ) VALUES ('1', 'Princess Street', 'Edinburgh', 'EH12 7AH', '00447856232254');	
INSERT INTO Branch(bID, street, city, p_code, bphone ) VALUES ('2', 'Queen Street', 'Edinburgh', 'EH12 8AH', '00447856232255');	 	
INSERT INTO Branch(bID, street, city, p_code, bphone ) VALUES ('3', 'Queen Street 2', 'Edinburgh', 'EH12 9AH', '00447856232256');
INSERT INTO Branch(bID, street, city, p_code, bphone ) VALUES ('4', 'Megan Street 2', 'Edinburgh', 'EH6 9HH', '00447856232257');
INSERT INTO Branch(bID, street, city, p_code, bphone ) VALUES ('5', 'Fountain Park 2', 'Edinburgh', 'EH8 9HA', '00447856232252');	

INSERT INTO Account(accNum, accType, bID, inRate, limitOfFreeOD, openDate) VALUES ('1', 'current', '1', '0.02', '200', to_date('2011/05/01','YYYY/MM/DD'));
INSERT INTO Account(accNum, accType, bID, inRate, limitOfFreeOD, openDate) VALUES ('2', 'current', '2', '0.02', '200', to_date('2011/06/01','YYYY/MM/DD'));	
INSERT INTO Account(accNum, accType, bID, inRate, limitOfFreeOD, openDate) VALUES ('3', 'saving', '3', '0.1', '800', to_date('2011/07/01','YYYY/MM/DD'));	
INSERT INTO Account(accNum, accType, bID, inRate, limitOfFreeOD, openDate) VALUES ('4', 'current', '4', '0.02', '200', to_date('2011/08/01','YYYY/MM/DD'));	
INSERT INTO Account(accNum, accType, bID, inRate, limitOfFreeOD, openDate) VALUES ('5', 'saving', '5', '0.1', '800', to_date('2011/09/01','YYYY/MM/DD'));

INSERT INTO Employee(empID, street, city, postCode, firstName, surName, empHomePhone, empMobile1, empMobile2, supervisorID, title, salary, niNum, bID, joinDate) VALUES ('1', 'street 1', 'Edinburgh', 'EH9 5AG', 'Adam', 'Sondler', '0044111111111', '0044111111121', '0044111111311', '5', 'HEAD', '60000', '1465789', '1', to_date('2001/09/01','YYYY/MM/DD'));
INSERT INTO Employee(empID, street, city, postCode, firstName, surName, empHomePhone, empMobile1, empMobile2, supervisorID, title, salary, niNum, bID, joinDate) VALUES ('2', 'street 2', 'Edinburgh', 'EH9 6AG', 'Harry', 'Donningston', '0044111111112', '0044111111131', '0044111111411', '1', 'Manager1', '30000', '1465780', '1', to_date('2003/09/01','YYYY/MM/DD'));
INSERT INTO Employee(empID, street, city, postCode, firstName, surName, empHomePhone, empMobile1, empMobile2, supervisorID, title, salary, niNum, bID, joinDate) VALUES ('3', 'street 3', 'Edinburgh', 'EH9 7AG', 'Sam', 'Brew', '0044111111113', '0044111111141', '0044111111511', '1', 'Manager2', '40000', '1465781', '1', to_date('2005/09/01','YYYY/MM/DD'));
INSERT INTO Employee(empID, street, city, postCode, firstName, surName, empHomePhone, empMobile1, empMobile2, supervisorID, title, salary, niNum, bID, joinDate) VALUES ('4', 'street 4', 'Edinburgh', 'EH9 8AG', 'Andrew', 'Cowell', '0044111111114', '0044111111151', '0044111111611', '1', 'Manager3', '50000', '1465782', '1', to_date('2005/09/01','YYYY/MM/DD'));
INSERT INTO Employee(empID, street, city, postCode, firstName, surName, empHomePhone, empMobile1, empMobile2, supervisorID, title, salary, niNum, bID, joinDate) VALUES ('5', 'street 5', 'Edinburgh', 'EH9 9AG', 'George', 'Morningstar', '0044111111115', '0044111111161', '0044111111711', '1', 'HEAD', '60000', '1465783', '2', to_date('2006/09/01','YYYY/MM/DD'));

INSERT INTO Customer(custID, street, city, postCode, firstName, surName, custHomePhone, custMobile1, custMobile2, niNum) VALUES ('10', 'street10', 'Edinburgh', 'EH15 5AF', 'Dean', 'Ambrose', '00441000000000', '00441000000001', '00441000000011', '111111');	
INSERT INTO Customer(custID, street, city, postCode, firstName, surName, custHomePhone, custMobile1, custMobile2, niNum) VALUES ('11', 'street11', 'Edinburgh', 'EH15 6AF', 'Paul', 'Hayman', '00442000000000', '00442000000002', '00442000000022', '111222');	
INSERT INTO Customer(custID, street, city, postCode, firstName, surName, custHomePhone, custMobile1, custMobile2, niNum) VALUES ('12', 'street12', 'Edinburgh', 'EH15 7AF', 'Christopher', 'Nolan', '00443000000000', '00443000000003', '00443000000033', '111333');	
INSERT INTO Customer(custID, street, city, postCode, firstName, surName, custHomePhone, custMobile1, custMobile2, niNum) VALUES ('13', 'street13', 'Edinburgh', 'EH15 8AF', 'Douglas', 'Colin', '00444000000000', '00444000000004', '00444000000044', '111444');	
INSERT INTO Customer(custID, street, city, postCode, firstName, surName, custHomePhone, custMobile1, custMobile2, niNum) VALUES ('14', 'street14', 'Edinburgh', 'EH15 9AF', 'Petar', 'Samprose', '00445000000000', '00445000000005', '00445000000055', '111555');	

INSERT INTO CustomerAccount(custID, accNum) VALUES ('10', '1');
INSERT INTO CustomerAccount(custID, accNum) VALUES ('11', '2');
INSERT INTO CustomerAccount(custID, accNum) VALUES ('12', '3');
INSERT INTO CustomerAccount(custID, accNum) VALUES ('13', '4');
INSERT INTO CustomerAccount(custID, accNum) VALUES ('14', '5');

Select surname from EMPLOYEE  WHERE surname LIKE '%on%';
Select firstName,surName from EMPLOYEE WHERE salary>40000;
Select firstName,surName, Branch.STREET from Branch join EMPLOYEE ON (Branch.BID = EMPLOYEE.BID);
Select firstName,surName from EMPLOYEE Where title LIKE '%HEAD%';
Select firstName,surName from EMPLOYEE Where salary>(Select SALARY from EMPLOYEE WHERE surname LIKE 'Brew');

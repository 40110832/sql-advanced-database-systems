   DROP Type employment_ref force;
   DROP Type Persontype force;
   DROP Type Namet force;
   DROP Type Phone force;
   DROP Type Address force;
   DROP Type job_ref force;
   Drop Table employment_ref_table;
   DROP Table employee_ref_table;
   DROP Table job_ref_table;
   
    CREATE TYPE Namet AS OBJECT (
    firstname VARCHAR2(30),
	middlename VARCHAR2(30),
    surname VARCHAR2(20) )
    not final;
	/
	
	CREATE TYPE Phone AS OBJECT (
    business Number,
	home Number,
    mobile Number)
    not final;
	/
		create type Address as object
    (street varchar2(20),
    city varchar2(20),
    postal_code varchar2(8))
    not final;
	/
	  create type Persontype as object(
	 pname Namet,
	 pphone Phone,
     paddress Address)
     not final;
    /
	create type employee_ref under Persontype () not final;
	/
	create table employee_ref_table of employee_ref;
	
INSERT INTO employee_ref_table VALUES (
 Namet('John', 'R', 'Smith'),
 Phone('1234567', NULL,'7374656'),
 Address('Morningside','Edinburgh', 'EH10 5DT'));
 
 INSERT INTO employee_ref_table VALUES (
 Namet('Mary', NULL, 'Miller'),
 Phone('3545643', '4535746','7334656'),
 Address('Princess St.','Edinburgh', 'EH1 3AB'));
 
INSERT INTO employee_ref_table VALUES (
 Namet('Mary', 'S', 'Miller'),
 Phone('3228484', NULL,'6452929'),
 Address('Merchiston Ave', 'Edinburgh','EH10 4AW'));
 
 create type job_ref as object (
 jobtitle varchar(20),
 salary_amount int,
 years_of_experience int );
 /
 
 create table job_ref_table of job_ref;
 
insert into job_ref_table values ('engineer', 30000,4);
insert into job_ref_table values ('programmer', 35000,3);
insert into job_ref_table values ('data analyst', 20000,15);
insert into job_ref_table values ('designer', 25000,2);
insert into job_ref_table values ('engineer', 33000,5);

create type employment_ref as object (
employee ref employee_ref,
position ref job_ref
);
/

create table employment_ref_table of employment_ref;

INSERT INTO employment_ref_table
 SELECT REF(e), REF(j)
 FROM job_ref_table j, employee_ref_table e
 WHERE e.pname.firstname = 'John'
 AND j.jobtitle = 'engineer';
 
INSERT INTO employment_ref_table
 SELECT REF(e), REF(j)
 FROM job_ref_table j, employee_ref_table e
 WHERE e.pname.firstname = 'Mary'
 AND j.jobtitle = 'designer';
 
  INSERT INTO employment_ref_table
 SELECT REF(e), REF(j)
 FROM job_ref_table j, employee_ref_table e
 WHERE e.pname.firstname = 'John'
 AND j.jobtitle = 'data analyst';
 
 SELECT e.employee.pname FROM employment_ref_table e;
 SELECT e.position.jobtitle FROM employment_ref_table e;
 SELECT e.employee.pname.firstname, e.position.salary_amount FROM employment_ref_table e;
 SELECT * FROM employment_ref_table e;
 
 select value(j)
from JOB_REF_TABLE j
where jobtitle = 'data analyst';

select * from JOB_REF_TABLE where jobtitle = 'engineer';

select ref(j)
from JOB_REF_TABLE j
where jobtitle = 'engineer';

select DEREF(e.employee)
from employment_ref_table e;

 SELECT e.employee.pname.firstname, e.position.salary_amount FROM employment_ref_table e WHERE e.position.salary_amount>30000;
 